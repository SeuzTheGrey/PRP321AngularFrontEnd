import { Component, OnInit } from '@angular/core';
import { AccountModel } from '../account.model';
import { LecturerModel } from '../lecturer';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from '../register.service';
import { ToastrService } from 'ngx-toastr'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  Account: AccountModel;
  Lecturer: LecturerModel;

  RegisterForm: NgForm;


  constructor(private registerService: RegisterService, private toastr: ToastrService, private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {

    //this.RegisterForm = this.formBuilder.group({
    //  UserName: this.formBuilder.control(''),
    //  Password: this.formBuilder.control(''),
    //  Priority: this.formBuilder.control(''),
    //  Adminstrator: this.formBuilder.control(''),
    //  FirstName: this.formBuilder.control(''),
    //  LastName: this.formBuilder.control(''),
    //  IDnumber: this.formBuilder.control(''),
    //  PhoneNumber: this.formBuilder.control(''),
    //  EmploymentDate: this.formBuilder.control(''),
    //  Email: this.formBuilder.control(''),
    //  ProfessionalLevel: this.formBuilder.control('')
    //})

  }




  OnSubmit(RegisterForm: NgForm) {

    this.Account.AccountPassword = RegisterForm.value.Password;
    this.Account.AccountPriority = RegisterForm.value.Priority;
    this.Account.AccountUsername = RegisterForm.value.UserName;
    this.Account.IsAdministrator = RegisterForm.value.Adminstrator;
    this.Lecturer.lecturerEmail = RegisterForm.value.Email;
    this.Lecturer.lecturerName = RegisterForm.value.FirstName;
    this.Lecturer.lecturerLastName = RegisterForm.value.LastName;
    this.Lecturer.lecturerIDNumber = RegisterForm.value.IDnumber;
    this.Lecturer.lecturerPhoneNumber = RegisterForm.value.PhoneNumber;
    this.Lecturer.lecturerEmploymentDate = RegisterForm.value.EmploymentDate;
    this.Lecturer.lecturerProfessionalLevel = RegisterForm.value.ProfessionalLevel;

    this.registerService.registerAccount(this.Account).subscribe((data: any) => {
      if (data.Succeded = true) {

        this.registerService.registerLecturer(this.Lecturer).subscribe((data: any) => {
          if (data.Succeded = true) {
            this.toastr.success('User Registration Successful');
          }
        })
      }
    })
  }


}
