import { Injectable } from '@angular/core';
import { AccountModel } from './account.model';
import { LecturerModel } from './lecturer';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  Account: AccountModel;
  Lecturer: LecturerModel;

  constructor(private http: HttpClient) { }
  // this is the actual request we make to the web api
  UserCliams() {
    return this.http.get('http://localhost:60300/api/GetClaims', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') }) })
  }

}
