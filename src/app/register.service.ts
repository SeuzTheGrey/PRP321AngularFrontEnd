import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountModel } from './account.model';
import { LecturerModel } from './lecturer';
import { Http, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { FormGroup, NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {



  constructor(private http: HttpClient) { }

  registerAccount(Account: AccountModel) {
    //this.Account.AccountPassword = Form.value.Password;
    //this.Account.AccountPriority = Form.value.Priority;
    //this.Account.AccountUsername = Form.value.UserName;
    //this.Account.IsAdministrator = Form.value.Adminstrator;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.post('http://localhost:60300/api/Account/Register', Account);
  }

  registerLecturer(Lecturer: LecturerModel) {


    //this.Lecturer.lecturerEmail = Form.value.Email;
    //this.Lecturer.lecturerName = Form.value.FirstName;
    //this.Lecturer.lecturerLastName = Form.value.LastName;
    //this.Lecturer.lecturerIDNumber = Form.value.IDnumber;
    //this.Lecturer.lecturerPhoneNumber = Form.value.PhoneNumber;
    //this.Lecturer.lecturerEmploymentDate = Form.value.EmploymentDate;
    //this.Lecturer.lecturerProfessionalLevel = Form.value.ProfessionalLevel;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.post('http://localhost:60300/api/Lecturer/Register', Lecturer);

  }
}
