export class LecturerModel {
  lecturerID: number;
  lecturerEmail: string;
  lecturerEmploymentDate: string;
  lecturerIDNumber: string;
  lecturerName: string;
  lecturerLastName: string;
  lecturerPhoneNumber: string;
  lecturerProfessionalLevel: string;

}
