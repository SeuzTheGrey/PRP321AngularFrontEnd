export class AccountModel {
  AccountPassword: string;
  AccountPriority: number;
  AccountUsername: string;
  IsAdministrator: number;
}
