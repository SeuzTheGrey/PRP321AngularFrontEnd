import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  //For Logout (Since we dont have anything for logout atm and the website isnt designed for and i dont know how to echo things based on local storage, there fore i will put the code here for future use)

  Logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/landing']);
  }

}
