import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavnologinComponent } from './navnologin.component';

describe('NavnologinComponent', () => {
  let component: NavnologinComponent;
  let fixture: ComponentFixture<NavnologinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavnologinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavnologinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
