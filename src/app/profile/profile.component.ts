import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  UserClaims: any;

  constructor(private user: UserService) { }

  ngOnInit() {
     //this will query the web api with a http get request from the user service and assign the value from the request to a variable
    this.user.UserCliams().subscribe((data: any) => {
      this.UserClaims = data;
    });
  }

}
