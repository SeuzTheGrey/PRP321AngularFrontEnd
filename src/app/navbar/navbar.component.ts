import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  UserClaims: any;

  constructor(private user: UserService) { }

  ngOnInit() {
    //this will query the web api with a http get request from the user service and assign the value from the request to a variable
    this.user.UserCliams().subscribe((data: any) => {
      this.UserClaims = data;
    });
  }

  //this to check whether a person is logged in and provide a bool to either hide or show login or logout
  LoginIn() {
    if (localStorage.getItem('userToken') == null) {
      return true;
    }
    else {
      return false;
    }
  }

  // this to check whether a user is an admin or not and hide admin link or not
  isAdmin() {
    if (this.UserClaims.isAdminstrator == true) {
      return true;
    } else {
      return false;
    }
  }



}
