import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router, private login: LoginService) { }

  ngOnInit() {
    //this.LoginForm = new FormGroup({

    //  Username: new FormControl(''),
    //  Password: new FormControl('')

    //})

    this.LoginForm = this.formBuilder.group({
      Username: this.formBuilder.control(''),
      Password: this.formBuilder.control('')
    })

  }




  OnSubmit(username, password) {

    this.login.userAuthentication(username, password).subscribe((data: any) => {
      localStorage.setItem('userToken', data.access_token);
      this.router.navigate(['/main']);
    })
  }

  

}
